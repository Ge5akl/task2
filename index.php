<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Второе тестовое задание</title>
    <link rel="stylesheet" href="styles/style.css">
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="jquery.form.js"></script>
 
<body>
<form id="FormaAjax" action="server.php" method="post">
  Имя: <input type="text" name="pole1" />
  Фамилия: <textarea name="pole2"> 
  </textarea>
  Возраст: <input type="text" name="pole3" />   
  <input type="submit" value="Отправить данные" />
</form>
<div id="result"></div>

<script type="text/javascript">
	$(document).ready(function(){
	       $("#FormaAjax").ajaxForm(function() {
               $("#result").load("server.php");
	   });
	});
</script>

<script src="scripts/index.js"></script>

</html>