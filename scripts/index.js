$(document).ready(function() {
    $("#addObject").click(function (e) {

        e.preventDefault();

        if($("#name").val()==="") 
        {
            alert("Введите текст!");
            return false;
        }

        var myData = "name="+ $("#name").val(); 
        jQuery.ajax({
            type: "POST", 
            url: "server.php", 
            dataType:"text",
            data:myData, 
            success:function(response){
            $("#responds").append(response);
            $("#name").val(''); 
            },
            error:function (xhr, ajaxOptions, thrownError){
                alert(thrownError);
            }
        });
    });
});